//app.js的内容
// 连接mongodb（必须）
var mongo = require('mongodb');
var monk = require('monk');
var db = monk('localhost:27017/nodetest1');

var routes = require('./routes');
var express = require('express');
var path = require('path');
var port=process.env.PORT || 3010;
var app = express();

//form提交表单，解析body的（必须）
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'views'));
app.engine('.html', require('ejs').renderFile);
app.set('view engine', 'html');


app.get('/', routes.index);
app.get('/new', routes.new);
app.get('/userlist', routes.userlist(db));
app.post('/adduser', routes.adduser(db));

app.listen(port);
console.log('movie is running on port:'+port);

module.exports = app;
