mongodb测试例子

1.安装mongodb
    npm install mongodb

2.配置路由
    在.app.js中
    app.use('/db',require('./routes/db'));
3.mongodb使用
    在.routes/db.js中

    var  mongodb = require('mongodb');
    var  server  = new mongodb.Server('localhost', 27017, {auto_reconnect:true});
    var  db = new mongodb.Db('school', server, {safe:true});
    var express = require('express');
    var router = express.Router();

    //连接db
    db.on("close", function (err,db) {
        if(err) throw err;
        else console.log("成功关闭数据库.");
    });

    router.get('/list', function(req, res, next) {
        db.open(function (err,db) {
            if(err)
                throw err;
            else{
                console.log("成功建立数据库连接");
                db.collection('test',{safe:true}, function(err, collection){
                    if(err){
                        console.log(err);
                    }else{

                        collection.find().toArray(function(err,docs){
                            console.log('查询所有记录：');
                            console.log(docs);
                            res.send(docs);
                        });

                    }
                });
            }
        });

    });

    module.exports = router;