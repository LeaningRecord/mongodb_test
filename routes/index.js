//.routes/index.js
//首页
exports.index = function(req, res){
    res.render('index', { title: 'index' });
};
//添加页
exports.new = function(req, res){
    res.render('new', { title: 'add_user' });
};
//列表页
exports.userlist = function(db) {
    return function(req, res) {
        var collection = db.get('usercollection');
        collection.find({},{},function(e,docs){
            res.render('list', {
                "title":"user_list",
                "userlist" : docs
            });
        });
    };
};

//接口
exports.adduser = function(db) {
    return function(req, res) {

        console.log(req.body);
        // Get our form values. These rely on the "name" attributes
        var userName = req.body.username;
        var userEmail = req.body.useremail;

        // Set our collection
        var collection = db.get('usercollection');

        // Submit to the DB
        collection.insert({
            "username" : userName,
            "email" : userEmail
        }, function (err, doc) {
            if (err) {
                // If it failed, return error
                res.send("There was a problem adding the information to the database.");
            }
            else {
                // And forward to success page
                res.redirect("/userlist");
            }
        });

    }
}